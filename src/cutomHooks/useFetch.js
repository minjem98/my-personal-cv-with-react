import { useEffect, useState } from "react";

const useFetch = (path, options = {}) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        // l'url vaut mieux qu'il soit placé dans un fichier .env et lui accedder avec process.env.URL_VALUE
        // pour simplifier les choses, je l'ai placé dans une simple variable
        let url = `http://localhost:8000${path}`;
        const resp = await fetch(url, options);
        const data = await resp.json();
        setData(data);
        setLoading(false);
      } catch (e) {
        setData([]);
        setLoading(false);
        setError(e);
      }
    };

    fetchData();
  }, []);
  return { data, loading, error };
};

export default useFetch;
