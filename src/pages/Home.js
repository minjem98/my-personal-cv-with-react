import React from "react";
import Header from "../components/Header";
import LeftSection from "../components/LeftSection";
import RightSection from "../components/RightSection";
import useFetch from "../cutomHooks/useFetch";
import "../styles/Home.css";
function Home() {
  let { data, loading, error } = useFetch("/infos");

  if (loading) return <div id="loading">Loading ...</div>;
  if (error) return <div>Something went wrong</div>;
  return (
    <div className="home-container">
      <Header headerInfos={data?.personalinfos} />
      <LeftSection leftSectionInfos={data?.educationalinfos} />
      <RightSection
        rightSectionInfos={{
          experiences: data?.experiences,
          projects: data?.projects,
          competences: data?.competences,
        }}
      />
    </div>
  );
}

export default Home;
