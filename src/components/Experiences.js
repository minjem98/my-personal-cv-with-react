import React from "react";
import Title from "./Title";
import { v4 as uuidv4 } from "uuid";
import PropTypes from "prop-types";

function Experience({ experiences }) {
  return (
    <div className="experiences-container">
      {experiences?.map(
        (
          {
            title,
            company,
            status,
            details,
            tools,
            startDate,
            endDate,
            companyLocation,
          },
          index
        ) => (
          <div key={uuidv4()} className="experiences-element">
            <div className="right-sections-subsections-container">
              <div className="right-sections-subsections-date">
                Du {startDate}
              </div>
              <div className="right-sections-subsections-date">
                Au {endDate}
              </div>
              <div className="right-sections-subsections-date">
                ({companyLocation})
              </div>
            </div>

            <div>
              <Title
                title={title}
                titleClassName="left-section-right-subsection-first-title"
              />
              <Title
                title={company}
                titleClassName="left-section-right-subsection-second-title"
              />
              <Title
                title={status}
                titleClassName="left-section-right-subsection-third-title"
              />
              <div>{details}</div>
              <div>
                <u>
                  <b>Outils utilisés: </b>
                </u>
                {tools}
              </div>
            </div>
          </div>
        )
      )}
    </div>
  );
}

export default Experience;
