import React from "react";
import "../styles/LeftSectionContainer.css";
import LeftSectionSubsection from "./LeftSectionSubsection";
import FormationElements from "./FormationElements";
import { v4 as uuidv4 } from "uuid";

function LeftSection({ leftSectionInfos }) {
  return (
    <div className="left-section-container">
      <LeftSectionSubsection title="PROFIL">
        <div id="profil-text">{leftSectionInfos?.profil}</div>
      </LeftSectionSubsection>

      <LeftSectionSubsection title="FORMATION">
        <FormationElements formations={leftSectionInfos?.formations} />
      </LeftSectionSubsection>

      <LeftSectionSubsection>
        <div className="contact-elements">
          <i className="fa fa-phone" aria-hidden="true"></i>
          {leftSectionInfos?.contacts?.phoneNumber}
        </div>
        <br />
        <div className="contact-elements">
          <i className="fa fa-envelope-o" aria-hidden="true"></i>
          {leftSectionInfos?.contacts?.email}
        </div>
        <br />
        <div className="contact-elements">
          <i className="fa fa-map-marker" aria-hidden="true"></i>
          {leftSectionInfos?.contacts?.city}
        </div>
        <br />
      </LeftSectionSubsection>

      <LeftSectionSubsection title="LANGUES">
        {leftSectionInfos?.langages.map(({ langage, level }, index) => (
          <div key={uuidv4()} className="langages-elements">
            {langage} : {level}
          </div>
        ))}
      </LeftSectionSubsection>

      <LeftSectionSubsection title="RÉSEAUX SOCIAUX">
        {leftSectionInfos?.socialMedias.map(({ link }, index) => (
          <div key={uuidv4()} className="social-media-elements">
            <i className="fa fa-linkedin-square" aria-hidden="true"></i>
            {link}
          </div>
        ))}
      </LeftSectionSubsection>
    </div>
  );
}

export default LeftSection;
