import React from "react";
import Title from "./Title";

function LeftSectionSubsection({ children, title = "" }) {
  return (
    <div className="leftsection-subsection-container">
      <Title title={title} titleClassName="sections-title" />
      <div> {children}</div>
      <br />
    </div>
  );
}

export default LeftSectionSubsection;
