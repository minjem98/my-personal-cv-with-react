import React from "react";
import "../styles/Header.css";
import personalImg from "../assets/images/pro-pic-2.png";
import Title from "./Title";

function Header({ headerInfos }) {
  return (
    <aside className="header-container">
      <div>
        <img src={personalImg} width="230px" height="250px" alt="" />
      </div>
      <div className="title-container">
        <Title title={headerInfos?.fullName} titleClassName="header-title-1" />
        <div id="name-underline"></div>
        <br />
        <Title
          title={headerInfos?.speciality}
          titleClassName="header-title-2"
        />
      </div>
    </aside>
  );
}

export default Header;
