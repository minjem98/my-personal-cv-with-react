import React from "react";
import Title from "./Title";
import "../styles/RightSectionContainer.css";
import { v4 as uuidv4 } from "uuid";

function Projects({ projects }) {
  // let {title,details,tools,startDate,endDate} = projects
  return (
    <div className="project-container">
      {projects?.map(({ title, details, tools, startDate, endDate }, index) => (
        <div key={uuidv4()} className="projects-element">
          <div className="right-sections-subsections-container">
            <div className="right-sections-subsections-date">
              Du {startDate}
            </div>
            <div className="right-sections-subsections-date">Au {endDate}</div>
          </div>

          <div>
            <Title
              title={title}
              titleClassName="left-section-right-subsection-first-title"
            />
            <div>{details}</div>
            <div>
              <u>
                <b>Outils utilisés: </b>
              </u>
              {tools}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}

export default Projects;
