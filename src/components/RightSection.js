import React from "react";
import "../styles/RightSectionContainer.css";
import Experiences from "./Experiences";
import Projects from "./Projects";
import RightSectionSubsection from "./RightSectionSubsection";
import { v4 as uuidv4 } from "uuid";

function RightSection({ rightSectionInfos }) {
  let { experiences, projects, competences } = rightSectionInfos;

  return (
    <div className="right-section-container">
      <RightSectionSubsection title="EXPÉRIENCES PROFESSIONNELLES">
        <Experiences experiences={experiences} />
      </RightSectionSubsection>

      <RightSectionSubsection title="PROJETS ACADÉMIQUES">
        <Projects projects={projects} />
      </RightSectionSubsection>

      <RightSectionSubsection title="COMPÉTENCES TECHNIQUES">
        {competences?.map(({ name, value }, index) => (
          <div className="competences-element" key={uuidv4()}>
            {" "}
            <span id="name-of-skill">{name} : </span> {value}{" "}
          </div>
        ))}
      </RightSectionSubsection>
    </div>
  );
}

export default RightSection;
