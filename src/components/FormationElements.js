import React from "react";
import { v4 as uuidv4 } from "uuid";

function FormationElements({ formations }) {
  return (
    <div>
      {formations?.map(({ date, city, value, school }, index) => (
        <div key={uuidv4()} className="formation-element-container">
          <div>
            <div className="formation-element-left-item">{date}</div>
            <div className="formation-element-left-item">{city}</div>
          </div>

          <div>
            <div className="formation-element-right-item">{value}</div>
            <div className="formation-element-right-item-school">{school}</div>
          </div>
        </div>
      ))}
    </div>
  );
}

export default FormationElements;
