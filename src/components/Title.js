import React from "react";

function Title({ title = "", titleClassName = "" }) {
  return <div className={titleClassName}>{title}</div>;
}

export default Title;
