import React from "react";
import Title from "./Title";

function RightSectionSubsection({ children, title = "" }) {
  return (
    <div className="rightsection-subsection-container">
      <Title title={title} titleClassName="sections-title" />
      <div> {children}</div>
      <br />
    </div>
  );
}

export default RightSectionSubsection;
